import bodyParser from 'body-parser';
//import cors from 'cors';
const cors = require('cors');
import express, { Request, Response, NextFunction } from 'express';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

app.get('/api/me', (req: Request, res: Response, next: NextFunction) => {
  res.json({me: 'mmh'});
});

export default app;