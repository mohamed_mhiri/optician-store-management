import app from './app';
import http from 'http';

const PORT = 5700;

app.set('port', PORT);

const server = http.createServer(app);

server.listen(PORT);