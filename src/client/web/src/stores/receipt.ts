import { defineStore } from 'pinia';

import receipts from '@/api/receipts';

export interface ReceiptsStoreState {
  receipts: {
    payed: Array<any>,
    notPayed: Array<any>,
    arranged: Array<any>
  },
}

export const useReceiptsStore = defineStore('receipts', {
  state: (): ReceiptsStoreState => ({
    receipts: {
      payed: [],
      notPayed: [],
      arranged: []
    },
  }),
  actions: {
    fetchReceipts ({ query }: any) {
      if (query === '') {
        receipts
          .get()
          .then(data => {
            this.receipts.payed = data.filter((rec: any) => (rec.remains === 0 && rec.paid === '0'))
            this.receipts.notPayed = data.filter((rec: any) => (rec.remains != 0 && rec.paid === '0'))
            this.receipts.arranged = data.filter((rec: any) => (rec.paid === '1'));
          });
      } else {
        receipts
          .search(query)
          .then(data => {
            this.receipts.payed = data.filter((rec: any) => (rec.remains === 0 && rec.paid === '0'))
            this.receipts.notPayed = data.filter((rec: any) => (rec.remains != 0 && rec.paid === '0'))
            this.receipts.arranged = data.filter((rec: any) => (rec.paid === '1'));
          });
      }
    },
    async createReceipt (createReceipt: any) {
      return receipts.create(createReceipt);
    },
    async updateReceipt (updateReceipt: any) {
      return receipts.update(updateReceipt);
    },
    async getReceipt (receiptId: any) {
      return receipts.getOne(receiptId);
    },
  },
});