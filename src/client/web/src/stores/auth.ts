import { defineStore } from 'pinia';

import auth from '@/api/status';

export const useAuthStore = defineStore('auth', {
  actions: {
    async closeConnection(state: boolean = false) {
      await auth.updateStatus(state);
    },
    isConnected() {
      return auth.getStatus();
    }
  }
});
