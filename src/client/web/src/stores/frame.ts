import { defineStore } from 'pinia';

import inventory from '@/api/inventory';

const sortOrder = (ent: any, elem: any) => {
  if (ent.label.toLowerCase() < elem.label.toLowerCase()) {
    return -1;
  }
  if (ent.label.toLowerCase() > elem.label.toLowerCase()) {
    return 1;
  }
  if ((ent.label.toLowerCase() === elem.label.toLowerCase()) && (+ent.price < +elem.price)) {
    return -1;
  }
  if ((ent.label.toLowerCase() === elem.label.toLowerCase()) && (+ent.price > +elem.price)) {
    return 1;
  }
  return 0;
};

const labelFilter = (frame: any, label: string) => {
  if (label.length === 0)
    return true;
  return frame.label.match(new RegExp(`^.*(${label}).*`, 'i'));
};

const categoryFilter = (frame: any, category: string) => {
  if (category.length === 0)
    return true;
  return frame.category === category;
};

const referenceFilter = (frame: any, reference: string) => {
  if (reference.length === 0)
    return true;
  return frame.reference.match(new RegExp(`^.*(${reference}).*`, 'i'));
};

const priceFilter = (frame: any, price: string) => {
  if (price.length === 0)
    return true;
  return frame.price.match(new RegExp(`^(${price})[0-9]*`));
};

const colorFilter = (frame: any, color: string) => {
  if (color.length === 0)
    return true;
  return frame.color.match(new RegExp(`^.*(${color}).*`, 'i'));
};

const sizeFilter = (frame: any, size: string) => {
  if (size.length === 0)
    return true;
  return frame.size.match(new RegExp(`^.*(${size}).*`, 'i'));
};

const isInitialState = (frameCriteria: any, state: any): boolean => {
  if (Object
    .values(frameCriteria)
    .every((item: any) => item.length === 0)
    && (state.allFrames.sunglasses.length === 0 &&
      state.allFrames.optical.length === 0)) {
    return true;
  } return false;
};

const isResetState = (frameCriteria: any, state: any): boolean => {
  if (Object
    .values(frameCriteria)
    .every((item: any) => item.length === 0)
    && (state.allFrames.sunglasses.length !== 0 &&
      state.allFrames.optical.length !== 0)) {
    return true;
  } return false;
};

export interface FramesStoreState {
  // frame store
  frames: {
    sunglasses: Array<any>,
    optical: Array<any>
  },
  allFrames: {
    sunglasses: Array<any>,
    optical: Array<any>
  },
  selectedFrames: Array<any>
}

const defaultState = {
  frames: {
    sunglasses: [],
    optical: []
  },
  allFrames: {
    sunglasses: [],
    optical: []
  },
  selectedFrames: []
};

export const useFramesStore = defineStore('frames', {
  state: (): FramesStoreState => ({ ...defaultState }),
  actions: {
    inventory(frameCriteria: any) {
      const { label, category, reference, price, size, color } = frameCriteria;
      if (isInitialState(frameCriteria, this)) {
        inventory
          .get()
          .then((data: any) => {
            this.allFrames.sunglasses = data.filter((fr: any) => fr.category === 'S').sort(sortOrder);
            this.allFrames.optical = data.filter((fr: any) => fr.category === 'O').sort(sortOrder);
          });
      } else if (isResetState([...label, ...reference, ...category, ...price, ...size, ...color], this)) {
        this.frames.sunglasses = [];
        this.frames.optical = [];
      }
      else {
        this.frames.sunglasses = this.allFrames.sunglasses
          .filter((fra: any) => {
            return labelFilter(fra, label)
              && categoryFilter(fra, category)
              && referenceFilter(fra, reference)
              && priceFilter(fra, price)
              && sizeFilter(fra, size)
              && colorFilter(fra, color)
          })
          .sort(sortOrder);
        this.frames.optical = this.allFrames.optical
          .filter((fra: any) => {
            return labelFilter(fra, label)
              && categoryFilter(fra, category)
              && referenceFilter(fra, reference)
              && priceFilter(fra, price)
              && sizeFilter(fra, size)
              && colorFilter(fra, color)
          })
          .sort(sortOrder);
      }

    },
    onSelectFrame(frame: any) {
      this.selectedFrames.push(frame);
      this.allFrames.optical = this.allFrames.optical.filter((item: any) => item._id != frame._id);
      this.allFrames.sunglasses = this.allFrames.sunglasses.filter((item: any) => item._id != frame._id);
      this.frames.optical = this.frames.optical.filter((item: any) => item._id != frame._id);
      this.frames.sunglasses = this.frames.sunglasses.filter((item: any) => item._id != frame._id);
    },
    onDeselectFrame(frame: any) {
      this.selectedFrames = this.selectedFrames.filter((item: any) => item._id != frame._id);
      this.frames = { sunglasses: [], optical: [] };
      if (frame.category === 'O' && !this.allFrames.optical.map((optical: any) => optical._id).includes(frame._id)) {
        this.allFrames.optical.push(frame);
        this.allFrames.optical.sort(sortOrder);
      }
      if (frame.category === 'S' && !this.allFrames.sunglasses.map((sunglass: any) => sunglass._id).includes(frame._id)) {
        this.allFrames.sunglasses.push(frame);
        this.allFrames.sunglasses.sort(sortOrder);
      }
    },
    async createFrame(data: any) {
      return inventory.post(data);
    },
    async getFrame(data: any) {
      return inventory.getOne(data);
    },
    clearFrames() {
      this.selectedFrames = [];
      this.frames = {
        sunglasses: [],
        optical: []
      };
      this.allFrames = {
        sunglasses: [],
        optical: []
      };
    }
  },
});