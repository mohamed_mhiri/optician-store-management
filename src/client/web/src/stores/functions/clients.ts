import type { ClientStoreState } from '../client';

export const sortOrder = (ent: any, elem: any) => {
  if (ent.lName.toLowerCase() < elem.lName.toLowerCase()) {
    return -1;
  }
  if (ent.lName.toLowerCase() > elem.lName.toLowerCase()) {
    return 1;
  }
  if ((ent.fName != undefined && elem.fName != undefined && ent.lName != undefined && elem.lName != undefined) && (ent.lName.toLowerCase() === elem.lName.toLowerCase()) && (ent.fName.toLowerCase() < elem.fName.toLowerCase())) {
    return -1;
  }
  if ((ent.fName != undefined && elem.fName != undefined && ent.lName != undefined && elem.lName != undefined) && (ent.lName.toLowerCase() === elem.lName.toLowerCase()) && (ent.fName.toLowerCase() > elem.fName.toLowerCase())) {
    return 1;
  }
  return 0;
};

export const nameFilter = (patient: any, name: string) => {
  if (name.length === 0)
    return true;
  return patient.fName.match(new RegExp(`^.*(${name}).*`, 'i'));
};

export const familyNameFilter = (patient: any, familyName: string) => {
  if (familyName.length === 0)
    return true;
  return patient.lName.match(new RegExp(`^.*(${familyName}).*`, 'i'));
};

export const glassFilter = (patient: any, glassCriteria: string) => {
  if (glassCriteria.length === 0)
    return true;
  return patient
    .glasses
    .some((glass: any) => glass.label.match(new RegExp(`^.*(${glassCriteria}).*`, 'i')));
};

export const lentilFilter = (patient: any, lentilCriteria: string) => {
  if (lentilCriteria.length === 0)
    return true;
  return patient.lentils
    .some((lentil: any) => {
      return lentil.label.match(new RegExp(`^.*(${lentilCriteria}).*`, 'i'));
    })
};

export const telFilter = (patient: any, telCriteria: string) => {
  if (telCriteria.length === 0)
    return true;
  return patient.entity.tel.some((tel: string) => tel.match(new RegExp(`^.*(${telCriteria}).*`, 'i')));
};

export const updatedDayFilter = (patient: any, updatedDay: string) => {
  if (updatedDay.length === 0) {
    return true;
  }
  const date = new Date(patient.updatedAt);
  return date.getDate() == +updatedDay;
}

export const updatedMonthFilter = (patient: any, updatedMonth: string) => {
  if (updatedMonth.length === 0) {
    return true;
  }
  const date = new Date(patient.updatedAt);
  return 1 + date.getMonth() == +updatedMonth;
}

export const updatedYearFilter = (patient: any, updatedYear: string) => {
  if (updatedYear.length === 0) {
    return true;
  }
  const date = new Date(patient.updatedAt);
  return date.getFullYear() == +updatedYear;
}

export const isInitialState = (state: ClientStoreState, patientCriteria: any) => {
  if (Object
    .values(patientCriteria)
    .every((item: any) => item.length === 0)
    && (state.allPatients.length === 0)) {
    return true;
  } return false;
};

export const isResetState = (state: ClientStoreState, patientCriteria: any): boolean => {
  if (Object
    .values(patientCriteria)
    .every((item: any) => item.length === 0)
    && (state.allPatients.length !== 0)) {
    return true;
  } return false;
};