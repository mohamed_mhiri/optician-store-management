import { defineStore } from 'pinia';

import { Route, ParentRoute, sidebarItems } from '@/router/routes';

export interface SidebarStoreState {
  // sidebar store
  links: Array<ParentRoute>,
}

export const useSidebarStore = defineStore('sidebar', {
  state: (): SidebarStoreState => ({
    links: sidebarItems,
  }),
  actions: {
    onSelectLink(selectedLink: Route) {
      console.log(selectedLink.getLabel());
      
      //const linkFromSelected = this.links.filter((link: Route) => selectedLink.getLabel().includes(link.getLabel()))[0];
      //console.log('store onSelectLink', linkFromSelected);
      
      //this.links.filter((link: Route) => selectedLink.getLabel().includes(link.getLabel()))[0].getChildRoutes().filter((route: Route) => route.getLabel() == selectedLink.getLabel()).cssClass = 'mm-active';
      
      //.cssClass = 'mm-active';
    }
  }
});
