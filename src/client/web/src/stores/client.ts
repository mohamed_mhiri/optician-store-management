import { defineStore } from 'pinia';

import patients from '@/api/patients';
import { 
  isInitialState,
  sortOrder,
  isResetState,
  nameFilter,
  familyNameFilter,
  glassFilter,
  telFilter,
  lentilFilter,
  updatedDayFilter,
  updatedMonthFilter,
  updatedYearFilter
} from './functions/clients';

import { PatientCriteria } from '../types/client';

export interface ClientStoreState {
  // client store
  clients: Array<any>,
  patients: Array<any>,
  allPatients: Array<any>,
  selectedClient: any,
}

export const useClientsStore = defineStore('clients', {
  state: (): ClientStoreState => ({
    clients: [],
    patients: [],
    allPatients: [],
    selectedClient: {},
  }),
  actions: {
    loadPatients (patientCriteria: PatientCriteria) {
      const { updatedDay, updatedMonth, updatedYear, name, familyName, glass, lentil, tel } = patientCriteria;

      if (isInitialState(this, patientCriteria)) {
        patients
          .get()
          .then(data => {
            this.allPatients = data.sort(sortOrder);
          });
      } else if (isResetState(this, [...updatedDay, ...updatedMonth, ...updatedYear, ...name, ...familyName, ...glass, ...lentil, ...tel])) {
        this.patients = [];
      }
      else {
        this.patients = this.allPatients
          .filter((patient: any) => {
            return nameFilter(patient, name)
              && familyNameFilter(patient, familyName)
              && glassFilter(patient, glass)
              && telFilter(patient, tel)
              && lentilFilter(patient, lentil)
              && updatedDayFilter(patient, updatedDay)
              && updatedMonthFilter(patient, updatedMonth)
              && updatedYearFilter(patient, updatedYear)
          })
          .sort(sortOrder);
        console.log(this.patients);
      }

    },
    onSelectClient (client: any) {
      this.selectedClient = client;
    },
    clearClients () {
      this.$reset()
    }
  }
});
