export function emptyEyeValues(receipt: any) {
  if ((receipt.entity.rCylinder == null) && (receipt.entity.rAdd == null) &&
    (receipt.entity.rAxis == null) && (receipt.entity.rSphere == null) &&
    (receipt.entity.rGap == null) && (receipt.entity.rHeight == null)) {
    return '';
  } else {
    return 'no';
  }
}

export function sameEyeValues(receipt: any) {
  let response = '';
  if ((receipt.entity.rCylinder === receipt.entity.lCylinder) && (receipt.entity.rAdd === receipt.entity.lAdd)
    && (receipt.entity.rAxis === receipt.entity.lAxis) && (receipt.entity.rSphere === receipt.entity.lSphere)
    && (receipt.entity.lGap === receipt.entity.rGap) && (receipt.entity.lHeight === receipt.entity.rHeight)) {
    response += `<div class="twelve-col small"><b>VL</b></div>
      <div class="twelve-col small"><b> OD/OG: </b>`;
    if ((receipt.entity.rCylinder && receipt.entity.rCylinder.length > 0)) {
      if (receipt.entity.rCylinder.includes('+') || receipt.entity.rCylinder.includes('-')) {
        response += `(${receipt.entity.rCylinder}`;
      } else {
        response += `(+${receipt.entity.rCylinder}`;
      }
      if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 2) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 2)) {
        response += ` `;
      } else if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 1) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 1)) {
        response += `0 `;
      } else {
        response += `.00 `;
      }
    }
    if ((receipt.entity.rAxis && receipt.entity.rAxis.length > 0)) {
      response += `Axe: ${receipt.entity.rAxis}) `;
    }
    if ((receipt.entity.rSphere && receipt.entity.rSphere.length > 0)) {
      if (receipt.entity.rSphere.includes('-') || receipt.entity.rSphere.includes('+') || receipt.entity.lSphere.toLowerCase() === 'plan') {
        response += `${receipt.entity.rSphere}`;
      } else if (receipt.entity.lSphere == 0) {
        response += 'Plan';
      } else {
        response += `+${receipt.entity.rSphere}`;
      }
      if ((receipt.entity.rSphere.includes('.') && receipt.entity.rSphere.split('.')[1].length == 2) || (receipt.entity.rSphere.includes(',') && receipt.entity.rSphere.split(',')[1].length == 2) || receipt.entity.lSphere.toLowerCase() === 'plan' || receipt.entity.lSphere == 0) {
        response += ` `;
      } else if ((receipt.entity.rSphere.includes('.') && receipt.entity.rSphere.split('.')[1].length == 1) || (receipt.entity.rSphere.includes(',') && receipt.entity.rSphere.split(',')[1].length == 1)) {
        response += `0 `;
      } else {
        response += `.00 `;
      }
    }
    if ((receipt.entity.rAdd && receipt.entity.rAdd.length > 0)) {
      if (receipt.entity.rAdd.includes('+') || receipt.entity.rAdd.includes('-')) {
        response += `ADD: ${receipt.entity.rAdd}`;
      } else {
        response += `ADD: +${receipt.entity.rAdd}`;
      }
      if ((receipt.entity.rAdd.includes('.') && receipt.entity.rAdd.split('.')[1].length === 2) || (receipt.entity.rAdd.includes(',') && receipt.entity.rAdd.split(',')[1].length === 2)) {
        response += ` `;
      } else if ((receipt.entity.rAdd.includes('.') && receipt.entity.rAdd.split('.')[1].length === 1) || (receipt.entity.rAdd.includes(',') && receipt.entity.rAdd.split(',')[1].length === 1)) {
        response += `0 `;
      } else {
        response += `.00 `;
      }
    }
    response += `</div>`;
    return response;
  } else {
    return 'diff';
  }
}

export function rightGapAndHeight(receipt: any) {
  let response = '<div class="four-col small">';
  if ((receipt.entity.rGap && receipt.entity.rGap.length > 0)) {
    response += `Ec: ${receipt.entity.rGap} `;
  }
  if ((receipt.entity.rHeight && receipt.entity.rHeight.length > 0)) {
    response += `H: ${receipt.entity.rHeight} `;
  }
  response += `</div>`;
  return response;
}

export function leftGapAndHeight(receipt: any) {
  let response = '<div class="four-col small">';
  if ((receipt.entity.lGap && receipt.entity.lGap.length > 0)) {
    response += `Ec: ${receipt.entity.lGap} `;
  }
  if ((receipt.entity.lHeight && receipt.entity.lHeight.length > 0)) {
    response += `H: ${receipt.entity.lHeight} `;
  }
  response += `</div>`;
  return response;
}

export function rightEyeValuesWithDom(receipt: any) {
  let response = '';
  if ((receipt.entity.rCylinder.length > 0) || (receipt.entity.rAdd.length > 0) ||
    (receipt.entity.rAxis.length > 0) || (receipt.entity.rSphere.length > 0) ||
    (receipt.entity.rGap.length > 0) || (receipt.entity.rHeight.length > 0)) {
    response += `<div class="twelve-col small"><b>VL</b></div>
      <div class="twelve-col small"><b> OD: </b>`;
    response += rightEyeValues(receipt);
    response += `</div>`;
    return response;
  } else {
    return response;
  }
}

export function rightEyeValues(receipt: any) {
  let response = '';
  if ((receipt.entity.rCylinder != null && receipt.entity.rCylinder.length > 0) && (receipt.entity.rAxis != null && receipt.entity.rAxis.length > 0)) {
    if (receipt.entity.rCylinder.includes('+') || receipt.entity.rCylinder.includes('-')) {
      response += `(${receipt.entity.rCylinder}`;
    } else {
      response += `(+${receipt.entity.rCylinder}`;
    }
    if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 2) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 2)) {
      response += ` Axe: ${receipt.entity.rAxis})`;
    } else if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 1) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 1)) {
      response += `0 Axe: ${receipt.entity.rAxis})`;
    } else {
      response += `.00 Axe: ${receipt.entity.rAxis})`;
    }
  } else {
    if ((receipt.entity.rAxis && receipt.entity.rAxis.length > 0)) {
      response += `Axe: ${receipt.entity.rAxis} `;
    }
    if ((receipt.entity.rCylinder && receipt.entity.rCylinder.length > 0)) {
      if (receipt.entity.rCylinder.includes('+') || receipt.entity.rCylinder.includes('-')) {
        response += `(${receipt.entity.rCylinder}`;
      } else {
        response += `(+${receipt.entity.rCylinder}`;
      }
      if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 2) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 2)) {
        response += ` `;
      } else if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 1) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 1)) {
        response += `0 `;
      } else {
        response += `.00 `;
      }
    }
  }
  if ((receipt.entity.rSphere && receipt.entity.rSphere.length > 0)) {
    if (receipt.entity.rSphere.includes('-') || receipt.entity.rSphere.includes('+') || receipt.entity.rSphere.toLowerCase() === 'plan') {
      response += `${receipt.entity.rSphere}`;
    } else if (receipt.entity.rSphere == 0) {
      response += 'Plan';
    } else {
      response += `+${receipt.entity.rSphere}`;
    }
    if ((receipt.entity.rSphere.includes('.') && receipt.entity.rSphere.split('.')[1].length == 2) || (receipt.entity.rSphere.includes(',') && receipt.entity.rSphere.split(',')[1].length == 2) || receipt.entity.rSphere.toLowerCase() === 'plan' || receipt.entity.rSphere == 0) {
      response += ` `;
    } else if ((receipt.entity.rSphere.includes('.') && receipt.entity.rSphere.split('.')[1].length == 1) || (receipt.entity.rSphere.includes(',') && receipt.entity.rSphere.split(',')[1].length == 1)) {
      response += `0 `;
    } else {
      response += `.00 `;
    }
  }
  if ((receipt.entity.rAdd && receipt.entity.rAdd.length > 0)) {
    if (receipt.entity.rAdd.includes('+') || receipt.entity.rAdd.includes('-')) {
      response += `ADD: ${receipt.entity.rAdd}`;
    } else {
      response += `ADD: +${receipt.entity.rAdd}`;
    }
    if ((receipt.entity.rAdd.includes('.') && receipt.entity.rAdd.split('.')[1].length === 2) || (receipt.entity.rAdd.includes(',') && receipt.entity.rAdd.split(',')[1].length === 2)) {
      response += ` `;
    } else if ((receipt.entity.rAdd.includes('.') && receipt.entity.rAdd.split('.')[1].length === 1) || (receipt.entity.rAdd.includes(',') && receipt.entity.rAdd.split(',')[1].length === 1)) {
      response += `0 `;
    } else {
      response += `.00 `;
    }

  }
  return response;
}

export function leftEyeValuesWithDom(receipt: any) {
  let response = '';
  if ((receipt.entity.lCylinder.length > 0) || (receipt.entity.lAdd.length > 0) || (receipt.entity.lAxis.length > 0) ||
    (receipt.entity.lSphere.length > 0) || (receipt.entity.lGap.length > 0) || (receipt.entity.lHeight.length > 0)) {
    response += `<div class="twelve-col small"><b> OG: </b>`;
    response += leftEyeValues(receipt);
    response += `</div>`;
    return response;
  } else {
    return response;
  }
}

export function leftEyeValues(receipt: any) {
  let response = '';

  if ((receipt.entity.lCylinder != null && receipt.entity.lCylinder.length > 0) && (receipt.entity.lAxis != null && receipt.entity.lAxis.length > 0)) {
    if (receipt.entity.lCylinder.includes('+') || receipt.entity.lCylinder.includes('-')) {
      response += `(${receipt.entity.lCylinder}`;
    } else {
      response += `(+${receipt.entity.lCylinder}`;
    }
    if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 2) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 2)) {
      response += ` Axe: ${receipt.entity.lAxis})`;
    } else if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 1) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 1)) {
      response += `0 Axe: ${receipt.entity.lAxis})`;
    } else {
      response += `.00 Axe: ${receipt.entity.lAxis})`;
    }
  } else {
    if ((receipt.entity.lAxis && receipt.entity.lAxis.length > 0)) {
      response += `Axe: ${receipt.entity.lAxis} `;
    }
    if ((receipt.entity.lCylinder && receipt.entity.lCylinder.length > 0)) {
      if (receipt.entity.lCylinder.includes('+') || receipt.entity.lCylinder.includes('-')) {
        response += `(${receipt.entity.lCylinder}`;
      } else {
        response += `(+${receipt.entity.lCylinder}`;
      }
      if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 2) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 2)) {
        response += ` `;
      } else if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 1) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 1)) {
        response += `0 `;
      } else {
        response += `.00 `;
      }
    }
  }
  if ((receipt.entity.lSphere && receipt.entity.lSphere.length > 0)) {
    if (receipt.entity.lSphere.includes('-') || receipt.entity.lSphere.includes('+') || receipt.entity.lSphere.toLowerCase() === 'plan') {
      response += `${receipt.entity.lSphere}`;
    } else if (receipt.entity.lSphere == 0) {
      response += 'Plan';
    } else {
      response += `+${receipt.entity.lSphere}`;
    }
    if ((receipt.entity.lSphere.includes('.') && receipt.entity.lSphere.split('.')[1].length == 2) || (receipt.entity.lSphere.includes(',') && receipt.entity.lSphere.split(',')[1].length == 2) || receipt.entity.lSphere.toLowerCase() === 'plan' || receipt.entity.lSphere == 0) {
      response += ` `;
    } else if ((receipt.entity.lSphere.includes('.') && receipt.entity.lSphere.split('.')[1].length == 1) || (receipt.entity.lSphere.includes(',') && receipt.entity.lSphere.split(',')[1].length == 1)) {
      response += `0 `;
    } else {
      response += `.00 `;
    }
  }
  if ((receipt.entity.lAdd && receipt.entity.lAdd.length > 0)) {
    if (receipt.entity.lAdd.includes('+') || receipt.entity.lAdd.includes('-')) {
      response += `ADD: ${receipt.entity.lAdd}`;
    } else {
      response += `ADD: +${receipt.entity.lAdd}`;
    }
    if ((receipt.entity.lAdd.includes('.') && receipt.entity.lAdd.split('.')[1].length === 2) || (receipt.entity.lAdd.includes(',') && receipt.entity.lAdd.split(',')[1].length === 2)) {
      response += ` `;
    } else if ((receipt.entity.lAdd.includes('.') && receipt.entity.lAdd.split('.')[1].length === 1) || (receipt.entity.lAdd.includes(',') && receipt.entity.lAdd.split(',')[1].length === 1)) {
      response += `0 `;
    } else {
      response += `.00 `;
    }

  }
  return response;
}

export function rightEyeCloseUpWithDom(receipt: any) {
  if (receipt.entity.rAdd == undefined || receipt.entity.rAdd == 0) {
    return '';
  } else {
    let sphAndAdd = 0;
    let response = `<div class="twelve-col"><b>VP</b></div>
      <div class="twelve-col small"><b> OD: </b>`;
    if ((receipt.entity.rCylinder != null && receipt.entity.rCylinder.length > 0) && (receipt.entity.rAxis != null && receipt.entity.rAxis.length > 0)) {
      if (receipt.entity.rCylinder.includes('+') || receipt.entity.rCylinder.includes('-')) {
        response += `(${receipt.entity.rCylinder}`;
      } else {
        response += `(+${receipt.entity.rCylinder}`;
      }
      if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 2) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 2)) {
        response += ` Axe: ${receipt.entity.rAxis})`;
      } else if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 1) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 1)) {
        response += `0 Axe: ${receipt.entity.rAxis})`;
      } else {
        response += `.00 Axe: ${receipt.entity.rAxis})`;
      }
    } else {
      if ((receipt.entity.rAxis && receipt.entity.rAxis.length > 0)) {
        response += `Axe: ${receipt.entity.rAxis} `;
      }
      if ((receipt.entity.rCylinder && receipt.entity.rCylinder.length > 0)) {
        if (receipt.entity.rCylinder.includes('+') || receipt.entity.rCylinder.includes('-')) {
          response += `(${receipt.entity.rCylinder}`;
        } else {
          response += `(+${receipt.entity.rCylinder}`;
        }
        if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 2) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 2)) {
          response += ` `;
        } else if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 1) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 1)) {
          response += `0 `;
        } else {
          response += `.00 `;
        }
      }
    }
    if ((receipt.entity.rSphere && receipt.entity.rSphere.length > 0)) {
      // split sphere by - or +
      // split add by + or -
      // sum both values
      if (receipt.entity.rSphere.includes('-')) {
        sphAndAdd += 0 - +(receipt.entity.rSphere.split('-')[1]);
      }
      else if (receipt.entity.rSphere.includes('+')) {
        sphAndAdd += +(receipt.entity.rSphere.split('+')[1]);
      }
      else if (receipt.entity.rSphere.toLowerCase() === 'plan' || receipt.entity.rSphere == 0) {
        sphAndAdd += 0;
      } else {
        sphAndAdd += +receipt.entity.rSphere;
      }
    }
    if ((receipt.entity.rAdd && receipt.entity.rAdd.length > 0)) {
      if (receipt.entity.rAdd.includes('+')) {
        sphAndAdd += +receipt.entity.rAdd.split('+')[1];
      } else if (receipt.entity.rAdd.includes('-')) {
        sphAndAdd -= +receipt.entity.rAdd.split('-')[1];
      } else {
        sphAndAdd += +receipt.entity.rAdd;
      }
    }
    if (sphAndAdd < 0) {
      response += `-${sphAndAdd.toFixed(2)}`
    } else if (sphAndAdd > 0) {
      response += `+${sphAndAdd.toFixed(2)}`
    } else {
      response += 'Plan';
    }
    response += `</div>`;
    return response;
  }
}

export function rightEyeCloseUp(receipt: any) {
  if (receipt.entity.rAdd == undefined || receipt.entity.rAdd == 0) {
    return '';
  } else {
    let response = '';
    let sphAndAdd = 0;
    if ((receipt.entity.rCylinder != null && receipt.entity.rCylinder.length > 0) && (receipt.entity.rAxis != null && receipt.entity.rAxis.length > 0)) {
      if (receipt.entity.rCylinder.includes('+') || receipt.entity.rCylinder.includes('-')) {
        response += `(${receipt.entity.rCylinder}`;
      } else {
        response += `(+${receipt.entity.rCylinder}`;
      }
      if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 2) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 2)) {
        response += ` Axe: ${receipt.entity.rAxis})`;
      } else if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 1) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 1)) {
        response += `0 Axe: ${receipt.entity.rAxis})`;
      } else {
        response += `.00 Axe: ${receipt.entity.rAxis})`;
      }
    } else {
      if ((receipt.entity.rAxis && receipt.entity.rAxis.length > 0)) {
        response += `Axe: ${receipt.entity.rAxis} `;
      }
      if ((receipt.entity.rCylinder && receipt.entity.rCylinder.length > 0)) {
        if (receipt.entity.rCylinder.includes('+') || receipt.entity.rCylinder.includes('-')) {
          response += `(${receipt.entity.rCylinder}`;
        } else {
          response += `(+${receipt.entity.rCylinder}`;
        }
        if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 2) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 2)) {
          response += ` `;
        } else if ((receipt.entity.rCylinder.includes('.') && receipt.entity.rCylinder.split('.')[1].length == 1) || (receipt.entity.rCylinder.includes(',') && receipt.entity.rCylinder.split(',')[1].length == 1)) {
          response += `0 `;
        } else {
          response += `.00 `;
        }
      }
    }
    if ((receipt.entity.rSphere && receipt.entity.rSphere.length > 0)) {
      // split sphere by - or +
      // split add by + or -
      // sum both values
      if (receipt.entity.rSphere.includes('-')) {
        sphAndAdd += 0 - +(receipt.entity.rSphere.split('-')[1]);
      }
      else if (receipt.entity.rSphere.includes('+')) {
        sphAndAdd += +(receipt.entity.rSphere.split('+')[1]);
      }
      else if (receipt.entity.rSphere.toLowerCase() === 'plan' || receipt.entity.rSphere == 0) {
        sphAndAdd += 0;
      } else {
        sphAndAdd += +receipt.entity.rSphere;
      }
    }
    if ((receipt.entity.rAdd && receipt.entity.rAdd.length > 0)) {
      if (receipt.entity.rAdd.includes('+')) {
        sphAndAdd += +receipt.entity.rAdd.split('+')[1];
      } else if (receipt.entity.rAdd.includes('-')) {
        sphAndAdd -= +receipt.entity.rAdd.split('-')[1];
      } else {
        sphAndAdd += +receipt.entity.rAdd;
      }
    }
    if (sphAndAdd < 0) {
      response += `-${sphAndAdd.toFixed(2)}`
    } else if (sphAndAdd > 0) {
      response += `+${sphAndAdd.toFixed(2)}`
    } else {
      response += 'Plan';
    }
    return response;
  }
}

export function leftEyeCloseUpWithDom(receipt: any) {
  if (receipt.entity.lAdd == undefined || receipt.entity.lAdd == 0) {
    return '';
  } else {
    let sphAndAdd = 0;
    let response = `<div class="twelve-col small"><b> OG: </b>`;
    if ((receipt.entity.lCylinder != null && receipt.entity.lCylinder.length > 0) && (receipt.entity.lAxis != null && receipt.entity.lAxis.length > 0)) {
      if (receipt.entity.lCylinder.includes('+') || receipt.entity.lCylinder.includes('-')) {
        response += `(${receipt.entity.lCylinder}`;
      } else {
        response += `(+${receipt.entity.lCylinder}`;
      }
      if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 2) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 2)) {
        response += ` Axe: ${receipt.entity.lAxis})`;
      } else if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 1) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 1)) {
        response += `0 Axe: ${receipt.entity.lAxis})`;
      } else {
        response += `.00 Axe: ${receipt.entity.lAxis})`;
      }
    } else {
      if ((receipt.entity.lAxis && receipt.entity.lAxis.length > 0)) {
        response += `Axe: ${receipt.entity.lAxis} `;
      }
      if ((receipt.entity.lCylinder && receipt.entity.lCylinder.length > 0)) {
        if (receipt.entity.lCylinder.includes('+') || receipt.entity.lCylinder.includes('-')) {
          response += `(${receipt.entity.lCylinder}`;
        } else {
          response += `(+${receipt.entity.lCylinder}`;
        }
        if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 2) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 2)) {
          response += ` `;
        } else if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 1) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 1)) {
          response += `0 `;
        } else {
          response += `.00 `;
        }
      }
    }
    if ((receipt.entity.lSphere && receipt.entity.lSphere.length > 0)) {
      // split sphere by - or +
      // split add by + or -
      // sum both values
      if (receipt.entity.lSphere.includes('-')) {
        sphAndAdd += 0 - +(receipt.entity.lSphere.split('-')[1]);
      }
      else if (receipt.entity.lSphere.includes('+')) {
        sphAndAdd += +(receipt.entity.lSphere.split('+')[1]);
      }
      else if (receipt.entity.lSphere.toLowerCase() === 'plan' || receipt.entity.lSphere == 0) {
        sphAndAdd += 0;
      } else {
        sphAndAdd += +receipt.entity.lSphere;
      }
    }
    if ((receipt.entity.lAdd && receipt.entity.lAdd.length > 0)) {
      if (receipt.entity.lAdd.includes('+')) {
        sphAndAdd += +receipt.entity.lAdd.split('+')[1];
      } else if (receipt.entity.lAdd.includes('-')) {
        sphAndAdd -= +receipt.entity.lAdd.split('-')[1];
      } else {
        sphAndAdd += +receipt.entity.lAdd;
      }
    }
    if (sphAndAdd < 0) {
      response += `-${sphAndAdd.toFixed(2)}`
    } else {
      response += `+${sphAndAdd.toFixed(2)}`
    }
    response += `</div>`;
    return response;
  }
}

export function leftEyeCloseUp(receipt: any) {
  if (receipt.entity.lAdd == undefined || receipt.entity.lAdd == 0) {
    return '';
  } else {
    let sphAndAdd = 0;
    let response = '';
    if ((receipt.entity.lCylinder != null && receipt.entity.lCylinder.length > 0) && (receipt.entity.lAxis != null && receipt.entity.lAxis.length > 0)) {
      if (receipt.entity.lCylinder.includes('+') || receipt.entity.lCylinder.includes('-')) {
        response += `(${receipt.entity.lCylinder}`;
      } else {
        response += `(+${receipt.entity.lCylinder}`;
      }
      if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 2) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 2)) {
        response += ` Axe: ${receipt.entity.lAxis})`;
      } else if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 1) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 1)) {
        response += `0 Axe: ${receipt.entity.lAxis})`;
      } else {
        response += `.00 Axe: ${receipt.entity.lAxis})`;
      }
    } else {
      if ((receipt.entity.lAxis && receipt.entity.lAxis.length > 0)) {
        response += `Axe: ${receipt.entity.lAxis} `;
      }
      if ((receipt.entity.lCylinder && receipt.entity.lCylinder.length > 0)) {
        if (receipt.entity.lCylinder.includes('+') || receipt.entity.lCylinder.includes('-')) {
          response += `(${receipt.entity.lCylinder}`;
        } else {
          response += `(+${receipt.entity.lCylinder}`;
        }
        if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 2) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 2)) {
          response += ` `;
        } else if ((receipt.entity.lCylinder.includes('.') && receipt.entity.lCylinder.split('.')[1].length == 1) || (receipt.entity.lCylinder.includes(',') && receipt.entity.lCylinder.split(',')[1].length == 1)) {
          response += `0 `;
        } else {
          response += `.00 `;
        }
      }
    }
    if ((receipt.entity.lSphere && receipt.entity.lSphere.length > 0)) {
      // split sphere by - or +
      // split add by + or -
      // sum both values
      if (receipt.entity.lSphere.includes('-')) {
        sphAndAdd += 0 - +(receipt.entity.lSphere.split('-')[1]);
      }
      else if (receipt.entity.lSphere.includes('+')) {
        sphAndAdd += +(receipt.entity.lSphere.split('+')[1]);
      }
      else if (receipt.entity.lSphere.toLowerCase() === 'plan' || receipt.entity.lSphere == 0) {
        sphAndAdd += 0;
      } else {
        sphAndAdd += +receipt.entity.lSphere;
      }
    }
    if ((receipt.entity.lAdd && receipt.entity.lAdd.length > 0)) {
      if (receipt.entity.lAdd.includes('+')) {
        sphAndAdd += +receipt.entity.lAdd.split('+')[1];
      } else if (receipt.entity.lAdd.includes('-')) {
        sphAndAdd -= +receipt.entity.lAdd.split('-')[1];
      } else {
        sphAndAdd += +receipt.entity.lAdd;
      }
    }
    if (sphAndAdd < 0) {
      response += `-${sphAndAdd.toFixed(2)}`
    } else {
      response += `+${sphAndAdd.toFixed(2)}`
    }
    return response;
  }
}

export function getEye(receipt: any) {
  console.log(rightEyeCloseUpWithDom(receipt));
  console.log(leftEyeCloseUpWithDom(receipt));
  if (emptyEyeValues(receipt) == '') {
    return '';
  } else {
    let response = '';
    if (sameEyeValues(receipt) != 'diff') {
      response = `${sameEyeValues(receipt)}${rightGapAndHeight(receipt)}`;
      return response;
    } else {
      response += `${rightEyeValuesWithDom(receipt)}${rightGapAndHeight(receipt)}`;
      response += `${leftEyeValuesWithDom(receipt)}${leftGapAndHeight(receipt)}`;
      if (receipt.entity.rAdd || receipt.entity.rAdd != 0) {
        response += `${rightEyeCloseUpWithDom(receipt)}`;
      }
      if (receipt.entity.lAdd || receipt.entity.lAdd != 0) {
        response += `${leftEyeCloseUpWithDom(receipt)}`;
      }
      return response;
    }
  }
}

export const getTel = (receipt: any) => {
  console.log(receipt.createdAt.toString().split(' '));
  let response = '';
  if (receipt.entity.tel.length > 0) {
    response = receipt.entity.tel
      .map((tel: any) => {
        return `<div class="sixteen-col">${tel}</div>`
      })
      .join('')
  }
  return response;
}

export const getFrame = (receipt: any) => {
  let response = '';
  if (receipt.frames.length > 0) {
    for (let i = 0; i < receipt.frames.length; i += 1) {
      response += `<div class="sixteen-col small">
      <b> #${i + 1}:</b>${receipt.frames[i].label}(${receipt.frames[i].category}) <b>Prix:</b> ${receipt.frames[i].price}</div>`;
    }
  }
  return response;
}

export const getAdminFrame = (receipt: any) => {
  let response = '';
  if (receipt.frames.length > 0) {
    for (let i = 0; i < receipt.frames.length; i += 1) {
      if (receipt.frames[i].size !== 'X') {
        response += `<div class="sixteen-col small">
        <b> #${i + 1}:</b> ${receipt.frames[i].label} (${receipt.frames[i].category} ${receipt.frames[i].reference} <b>Col:</b>${receipt.frames[i].color} <b>T:</b>${receipt.frames[i].size}) <b>Prix:</b> ${receipt.frames[i].price}</div>`;
      } else {
        response += `<div class="sixteen-col small">
        <b> #${i + 1}:</b> ${receipt.frames[i].label} (${receipt.frames[i].category} ${receipt.frames[i].reference} <b>Col:</b>${receipt.frames[i].color}) <b>Prix:</b> ${receipt.frames[i].price}</div>`;
      }
    }
  }
  return response;
}

export const getLentil = (receipt: any) => {
  let response = '';
  if (receipt.lentils.length > 0) {
    for (let i = 0; i < receipt.lentils.length; i += 1) {
      response += `<div class="sixteen-col small"><b>#${i + 1}:</b> ${receipt.lentils[i].label} <b>Prix:</b> ${receipt.lentils[i].price}</div>`;
    }
  }
  return response;
}

export const getGlass = (receipt: any) => {
  let response = '';
  if (receipt.glasses.length > 0) {
    for (let i = 0; i < receipt.glasses.length; i += 1) {
      response += `<div class="sixteen-col small"><b>#${i + 1}:</b> ${receipt.glasses[i].label} <b>Prix:</b> ${receipt.glasses[i].price}</div>`;
    }
  }
  return response;
}

const saleByFormatter = (finalStr: any, newStr: any) => finalStr + ' + ' + newStr;

const getSaleBy = (saleBy: any) => saleBy.length === 1 && saleBy[0] === '1' ? '1 + 1' : saleBy.reduce(saleByFormatter);

export const getTotal = (receipt: any) => `<div class="sixteen-col"><b>Total:</b> ${receipt.total}</div>
      <div class="sixteen-col"><b>Total avec remise:</b> ${receipt.totalWithDiscount}</div>
      <div class="eight-col"><b>Accompte:</b> ${receipt.downPayment}</div>
      <div class="eight-col"><b>Vendeur:</b> ${getSaleBy(receipt.saleBy)}</div>
      <div class="eight-col"><b>Reste:</b> ${receipt.remains}</div>
      <div class="eight-col paid"><b>${receipt.remains == 0 ? 'Payé' : 'Non Payé'}</b></div>
      <div class="eight-col"><b>Paiement:</b> ${receipt.paymentMethod}</div>`;

export function getPrintTxt(receipt: any) {
  let tel = '';
  if (getTel(receipt) !== '') {
    tel = `<div class="sixteen-col">
        <b>Tel:</b>
      </div>${getTel(receipt)}`;
  }
  let eye = '';
  if (getEye(receipt) !== '') {
    eye = `${getEye(receipt)}`;
  }
  let frame = '';
  if (getFrame(receipt) !== '') {
    frame = `<div class="sixteen-col"><b>Montures:</b></div>
        ${getFrame(receipt)}`;
  }
  let adminFrame = '';
  if (getAdminFrame(receipt) !== '') {
    adminFrame = `<div class="sixteen-col"><b>Montures:</b></div>
        ${getAdminFrame(receipt)}`;
  }
  let glass = '';
  if (getGlass(receipt) !== '') {
    glass = `<div class="sixteen-col">
        <b>Verres:</b>
      </div>
      ${getGlass(receipt)}`;
  }
  let lentil = '';
  if (getLentil(receipt) !== '') {
    lentil = `<div class="sixteen-col">
        <b>Lentilles:</b>
      </div>
      ${getLentil(receipt)}`;
  }
  let total = '';
  if (getTotal(receipt) !== '') {
    total = `${getTotal(receipt)}`;
  }

  const now = `Date: ${receipt.createdAt.toString().split(' ')[0].split('-')[2].split('T')[0]}/${receipt.createdAt.toString().split(' ')[0].split('-')[1]}/${receipt.createdAt.toString().split(' ')[0].split('-')[0]} ${+receipt.createdAt.toString().split(' ')[0].split(':')[0].split('T')[1] + 1}:${receipt.createdAt.toString().split(' ')[0].split(':')[1]}`;
  const client = 'Client';
  const history = 'Historique';
  const store = 'Atelier';
  const box = 'Caisse';
  const page = (type: any) => `<div class="client voucher">
      <div class="nine-col title">Reçu ${type} #${receipt.nbr}</div>
      <div class="seven-col date-title">${now}</div>
      <div class="sixteen-col"></div>
      <div class="sixteen-col store-data">
        OMAR ALOULOU</br>
        <b>Tel:</b> 24 161 987</br>
        38 Galerie Colisée Menzeh 6</br>
        <b>Email</b>: aloulouoptic@gmail.com
      </div>
      <div class="sixteen-col"></div>
      <div class="eight-col large"><b>Nom: </b>${receipt.lName}
      </div>
      <div class="eight-col large"><b>Prénom: </b>${receipt.fName}
      </div>
      ${tel}
      ${eye}
      ${frame}
      ${glass}
      ${lentil}
      <div class="sixteen-col line"></div>
      ${total}</div><div class="seperator1"></div>`;


  const adminPage = (type: any) => `
      <div class="nine-col title">Reçu ${type} #${receipt.nbr}</div>
      <div class="seven-col date-title">${now}</div>
      <div class="sixteen-col"></div>
      <div class="sixteen-col store-data">
        OMAR ALOULOU</br>
        <b>Tel:</b> 24 161 987</br>
        38 Galerie Colisée Menzeh 6</br>
        <b>Email</b>: aloulouoptic@gmail.com
      </div>
      <div class="sixteen-col"></div>
      <div class="eight-col large"><b>Nom: </b>${receipt.lName}
      </div>
      <div class="eight-col large"><b>Prénom: </b>${receipt.fName}
      </div>
      ${tel}
      ${eye}
      ${adminFrame}
      ${glass}
      ${lentil}
      <div class="sixteen-col line"></div>
      ${total}`;
  return `${page(client)}<div class="store voucher">${adminPage(store)}</div><div class="seperator2"></div><div class="history voucher">${adminPage(history)}</div><div class="seperator3"></div>
      <div class="box voucher">
      <div class="sixteen-col box-title">Reçu Caisse #${receipt.nbr}</div>
      <div class="sixteen-col date-title" style="font-size:12px">${now}</div>
      <div class="sixteen-col"><b>Total:</b> ${receipt.total}</div>
      <div class="sixteen-col"><b>Total avec remise:</b> ${receipt.totalWithDiscount}</div>
      <div class="sixteen-col"><b>Accompte:</b> ${receipt.downPayment}</div>
      <div class="sixteen-col"><b>Reste:</b> ${receipt.remains}</div>
      <div class="sixteen-col"><b>Payement:</b> ${receipt.paymentMethod}</div>
      <div class="sixteen-col">${receipt.remains == 0 ? 'Payé' : 'Non Payé'}</div>
      <div class="sixteen-col"><b>Vendeur:</b> ${getSaleBy(receipt.saleBy)}</div>
    </div>`/*${total}`*/;
}
