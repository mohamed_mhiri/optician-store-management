export default async function getFrames () {
  const data = await fetch('http://localhost:5700/api/frame/getAll');
  return data.json();  
}