import { Receipt } from "../../types/receipt";

export function toOldModel(receipt: any) {
  const result = {
    data: {
      // client
      rCylinder: receipt.client.rightEye.cylinder,
      rAxis: receipt.client.rightEye.axis,
      rSphere: receipt.client.rightEye.sphere,
      lCylinder: receipt.client.leftEye.cylinder,
      lAxis: receipt.client.leftEye.axis,
      lSphere: receipt.client.leftEye.sphere,
      rAdd: receipt.client.rightEye.addition,
      lGap: receipt.client.leftEye.gap,
      lHeight: receipt.client.leftEye.height,
      lAdd: receipt.client.leftEye.addition,
      rGap: receipt.client.rightEye.gap,
      rHeight: receipt.client.rightEye.height,
      tel: receipt.client.telNumbers,
      fName: receipt.client.name,
      lName: receipt.client.familyName,
      // payment
      downPayment: receipt.downPayment,
      total: receipt.total,
      totalWithDiscount: receipt.totalWithDiscount,
      remains: receipt.remaining,
      paymentMethod: receipt.paymentMethods.length > 1 ? receipt.paymentMethods.reduce((acc = '', curr: string) => `${acc} + ${curr}`) : receipt.paymentMethods[0],
      saleBy: receipt.sellers,
      // products
      lentils: receipt.lentils.map((l: any) => { return {label: l.category ? `${l.label} ${l.category}` : l.label, price: l.price}; } ),
      glasses: receipt.glasses.map((g: any) => { return {label: g.category ? `${g.label} ${g.category}` : g.label, price: g.price}; } ),
      _id: receipt._id
    },
    newFrames: receipt.frames,
    oldFrames: []
  };
  if (!receipt._id) {
    delete result.data._id;
  }
  return result;
}

export function toNewModel(receipt: Receipt) {
  let result = {
    receipt: {
      client: {
        familyName: receipt.lName,
        name: receipt.fName,
        telNumbers: receipt.entity.tel,
        rightEye: {
          cylinder: receipt.entity.rCylinder,
          axis: receipt.entity.rAxis,
          addition: receipt.entity.rAdd,
          sphere: receipt.entity.rSphere,
          height: receipt.entity.rHeight,
          gap: receipt.entity.rGap
        },
        leftEye: {
          cylinder: receipt.entity.lCylinder,
          axis: receipt.entity.lAxis,
          addition: receipt.entity.lAdd,
          sphere: receipt.entity.lSphere,
          height: receipt.entity.lHeight,
          gap: receipt.entity.lGap
        },
      },      
      total: receipt.total,
      totalWithDiscount: receipt.totalWithDiscount,
      downPayment: receipt.downPayment,
      remaining: receipt.remains,
      paymentMethods: receipt.paymentMethod.split(' + '),
      sellers: receipt.saleBy,
      frames: receipt.frames,
      glasses: receipt.glasses,
      lentils: receipt.lentils,
      _id: receipt._id
    },
    frames: receipt.frames,
    glasses: receipt.glasses,
    lentils: receipt.lentils, 
  };
  if (!receipt._id) {
    delete result.receipt._id;
  }
  return result;
}