export const dateFormatter = (value: any) => {
  if (!value) return '';
  const tmp = value.toString().split('T')[0];
  return `${tmp.split('-')[2]}/${tmp.split('-')[1]}/${tmp.split('-')[0]}`;
}