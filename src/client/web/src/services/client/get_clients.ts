export default async function getClients () {
  const data = await fetch('http://localhost:5700/api/client/getAll');
  return data.json();  
}