export const dateFormatter = (value: any) => {
  if (!value) return '';
  const tmp = value.toString().split('T')[0];
  return `${tmp.split('-')[2]}/${tmp.split('-')[1]}/${tmp.split('-')[0]}`;
}

export const frameFormatter = (val: any) => {
  if (val === undefined) {
    return '';
  }
  let items = '';
  val.map((item: any) => items += `${item.label}(${item.category}-${item.price}) `);
  return items;
}

export const itemFormatter = (val: any) => {
  if (val === undefined) {
    return '';
  }
  let items = '';
  val.map((item: any) => items += `${item.label}<br>`);
  return items;
}

export const telFormatter = (val: any) => {
  if (val === undefined) {
    return '';
  }
  let items = '';
  val.map((item: any) => items += `${item}<br>`);
  return items;
}

export const eyeFormatter = (entity: any) => {
  if ((entity.rCylinder == null) && (entity.rAdd == null) && (entity.rAxis == null)
    && (entity.rSphere == null) && (entity.rGap == null) && (entity.rHeight == null)) {
    return ['', '', ''];
  }
  let response: Array<string> = [];
  if ((entity.rCylinder === entity.lCylinder) && (entity.rAdd === entity.lAdd) &&
    (entity.rAxis === entity.lAxis) && (entity.rSphere === entity.lSphere) &&
    (entity.rGap === entity.lGap) && (entity.rHeight === entity.lHeight)) {
    response = ['', '', '']
    response[0] += '<b>OD/OG: </b>';
    if ((entity.rCylinder)) {
      response[0] += `Cyl: ${entity.rCylinder} `;
    }
    if ((entity.rAxis)) {
      response[0] += `Axe: ${entity.rAxis} `;
    }
    if ((entity.rSphere)) {
      response[0] += `${entity.rSphere} `;
    }
    if ((entity.rGap)) {
      response[2] += `<b>OD/OG: </b>Ec: ${entity.rGap} `;
    }
    if ((entity.rHeight)) {
      response[2] += `H: ${entity.rHeight} `;
    }
    return response;
  }
  if ((entity.rCylinder != null) ||
    (entity.rAxis != null) || (entity.rSphere != null) ||
    (entity.rGap != null) || (entity.rHeight != null)) {
      response = ['', '', '']
      response[0] += '<b>OD: </b>';
    if ((entity.rCylinder)) {
      response[0] += `Cyl: ${entity.rCylinder} `;
    }
    if ((entity.rAxis)) {
      response[0] += `Axe: ${entity.rAxis} `;
    }
    if ((entity.rSphere)) {
      response[0] += `${entity.rSphere} `;
    }
    if ((entity.rGap)) {
      response[2] += `<b>OD: </b>Ec: ${entity.rGap} `;
    }
    if ((entity.rHeight)) {
      response[2] += `H: ${entity.rHeight} `;
    }
  }
  if ((entity.lCylinder != null) ||
    (entity.lAxis != null) || (entity.lSphere != null) ||
    (entity.lGap != null) || (entity.lHeight != null)) {

      response[0] += '<br><br><b>OG: </b>';
    if ((entity.lCylinder)) {
      response[0] += `Cyl: ${entity.lCylinder} `;
    }
    if ((entity.lAxis)) {
      response[0] += `Axe: ${entity.lAxis} `;
    }
    if ((entity.lSphere)) {
      response[0] += `${entity.lSphere} `;
    }
    if ((entity.lGap)) {
      response[2] += `<br><br><b>OG: </b>Ec: ${entity.lGap} `;
    }
    if ((entity.lHeight)) {
      response[2] += `H: ${entity.lHeight} `;
    }
  }
  if ((entity.rAdd != null) || (entity.lAdd != null)) {
    if (entity.rAdd === entity.lAdd) {
      response[1] += `<b>ADD: </b>${entity.lAdd} `;
    } else {
      if ((entity.rAdd != null)) {
        response[1] += `<b>OD: </b>${entity.rAdd} `;
      }
      if ((entity.lAdd != null)) {
        response[1] += `<br><br><b>OG: </b>${entity.lAdd} `;
      }
    }
  }
  return response;
}
