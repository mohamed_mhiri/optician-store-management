import Clients from '@/views/Clients.vue';
import Frames from '@/views/Frames.vue';
import NewFrame from '@/views/NewFrame.vue';
import NewReceipt from '@/views/NewReceipt.vue';
import Receipts from '@/views/Receipts.vue';
import Statistics from '@/views/Statistics.vue';
import Scanner from '@/views/Scanner.vue';

export class ParentRoute {
  constructor(childRoutes: Array<Route>, label: string) {
    this.childRoutes = childRoutes;
    this.label = label;
  }

  private childRoutes: Array<Route>;
  private label: string = '';

  public getChildRoutes() {
    return this.childRoutes;
  }

  public getLabel() {
    return this.label;
  }

}

export class Route {
  constructor(path: string, name: string, component: any, label: string, icon: string, cssClass: string) {
    this.path = path;
    this.name = name;
    this.component = component;
    this.label = label;
    this.icon = icon;
    this.cssClass = cssClass;
  }

  private path: string = '/';
  private name: string = '';
  private component: any;
  private label: string = '';
  private icon: string = '';
  private cssClass: string = '';
  public getLabel () {
    return this.label;
  }
  public getPath () {
    return this.path;
  }
  public getName () {
    return this.name;
  }
  public getComponent () {
    return this.component;
  }
  public getIcon () {
    return this.icon;
  }
  public getCssClass () {
    return this.cssClass;
  }
  public setCssClass (cssClass: string) {
    this.cssClass = cssClass;
  }
}

export const sidebarItems: ParentRoute[] = [
  new ParentRoute([
    new Route('/clients', 'Clients', Clients, 'Fiche des Clients', 'fa fa-users', 'mm-inactive')
  ], 'Clients' ),
  new ParentRoute([
    new Route('/frames', 'Frames', Frames, 'Cadres', 'fas fa-glasses', 'mm-inactive'),
    new Route('/new-frame', 'New Frame', NewFrame, 'Ajout Cadre', 'fas fa-glasses', 'mm-inactive'),
  ], 'Cadres' ),
  new ParentRoute([
    new Route('/receipts', 'Receipts', Receipts, 'Reçus', 'fas fa-folder-open', 'mm-inactive'),
    new Route('/new-receipt', 'New Receipt', NewReceipt, 'Ajout de Reçu(s)', 'fas fa-file-medical', 'mm-inactive'),
  ], 'Reçus' ),
  new ParentRoute([
    new Route('/stats', 'Statistics', Statistics, 'Statistiques', 'fas fa-chart-bar', 'mm-inactive')
  ], 'Statistiques' ),
  new ParentRoute([
    new Route('/scanner', 'Scanner', Scanner, 'Scanner', 'fas fa-chart-bar', 'mm-inactive')
  ], 'Scanner' ),
];