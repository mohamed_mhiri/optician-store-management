import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import { sidebarItems } from './routes';

import Receipt from '@/views/Receipt.vue';
import EditReceipt from '@/views/EditReceipt.vue';

const routes: Array<RouteRecordRaw> = sidebarItems
.map(item => item.getChildRoutes())
.flat()
.map(item => {
  return {
    path: item.getPath(),
    name: item.getName(),
    label: item.getLabel(),
    component: item.getComponent(),
    cssClass: item.getCssClass(),
  };
});

const hiddenRoutes = [
  { path: '/receipt/:receiptId', component: Receipt, name: 'ViewReceipt' },
  { path: '/edit-receipt/:receiptId', component: EditReceipt, name: 'EditReceipt' },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: [...routes, ...hiddenRoutes]
});

export default router;
