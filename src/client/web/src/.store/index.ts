import { createStore } from 'vuex';
import search from './modules/search/index';
import clients from './modules/clients/index';
import frames from './modules/frames/index';
import receipts from './modules/receipts/index';
import sidebar from './modules/layout/sidebar/index';

const store = createStore({
  modules: {
    search,
    clients,
    frames,
    receipts,
    sidebar
  }
})
export default store;
