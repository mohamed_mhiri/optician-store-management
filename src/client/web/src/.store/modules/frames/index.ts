import inventory from '@/api/inventory';

const sortOrder = (ent: any, elem: any) => {
  if (ent.label.toLowerCase() < elem.label.toLowerCase()) {
    return -1;
  }
  if (ent.label.toLowerCase() > elem.label.toLowerCase()) {
    return 1;
  }
  if ((ent.label.toLowerCase() === elem.label.toLowerCase()) && (+ent.price < +elem.price)) {
    return -1;
  }
  if ((ent.label.toLowerCase() === elem.label.toLowerCase()) && (+ent.price > +elem.price)) {
    return 1;
  }
  return 0;
};

const brandFilter = (frame: any, brand: string) => {
  if (brand.length === 0)
    return true;
  return frame.label.match(new RegExp(`^.*(${brand}).*`, 'i'));
};

const categoryFilter = (frame: any, category: string) => {
  if (category.length === 0)
    return true;
  return frame.category === category;
};

const referenceFilter = (frame: any, reference: string) => {
  if (reference.length === 0)
    return true;
  return frame.reference.match(new RegExp(`^.*(${reference}).*`, 'i'));
};

const priceFilter = (frame: any, price: string) => {
  if (price.length === 0)
    return true;
  return frame.price.match(new RegExp(`^(${price})[0-9]*`));
};

const colorFilter = (frame: any, color: string) => {
  if (color.length === 0)
    return true;
  return frame.color.match(new RegExp(`^.*(${color}).*`, 'i'));
};

const sizeFilter = (frame: any, size: string) => {
  if (size.length === 0)
    return true;
  return frame.size.match(new RegExp(`^.*(${size}).*`, 'i'));
};

const isInitialState = (frameCriteria: any, state: any): boolean => {
  if (Object
    .values(frameCriteria)
    .every((item: any) => item.length === 0)
    && (state.allFrames.sunglasses.length === 0 &&
      state.allFrames.optical.length === 0)) {
    return true;
  } return false;
};

const isResetState = (frameCriteria: any, state: any): boolean => {
  if (Object
    .values(frameCriteria)
    .every((item: any) => item.length === 0)
    && (state.allFrames.sunglasses.length !== 0 &&
      state.allFrames.optical.length !== 0)) {
    return true;
  } return false;
};

export default {
  state() {
    return {
      frames: {
        sunglasses: [] as any,
        optical: [] as any
      },
      allFrames: {
        sunglasses: [] as any,
        optical: [] as any
      },
      selectedFrames: [] as any
    }
  },
  mutations: {
    inventory: (state: any, frameCriteria: any) => {
      const { brand, category, reference, price, size, color } = frameCriteria;
      if (isInitialState(frameCriteria, state)) {
        inventory
          .get()
          .then((data: any) => {
            state.allFrames.sunglasses = data.filter((fr: any) => fr.category === 'S').sort(sortOrder);
            state.allFrames.optical = data.filter((fr: any) => fr.category === 'O').sort(sortOrder);
          });
      } else if (isResetState([...brand, ...reference, ...category, ...price, ...size, ...color], state)) {
        state.frames.sunglasses = [];
        state.frames.optical = [];
      }
      else {
        state.frames.sunglasses = state.allFrames.sunglasses
          .filter((fra: any) => {
            return brandFilter(fra, brand)
              && categoryFilter(fra, category)
              && referenceFilter(fra, reference)
              && priceFilter(fra, price)
              && sizeFilter(fra, size)
              && colorFilter(fra, color)
          })
          .sort(sortOrder);
        state.frames.optical = state.allFrames.optical
          .filter((fra: any) => {
            return brandFilter(fra, brand)
              && categoryFilter(fra, category)
              && referenceFilter(fra, reference)
              && priceFilter(fra, price)
              && sizeFilter(fra, size)
              && colorFilter(fra, color)
          })
          .sort(sortOrder);
      }

    },
    onSelectFrame: (state: any, frame: any) => {
      state.selectedFrames.push(frame);
      state.allFrames.optical = state.allFrames.optical.filter((item: any) => item._id != frame._id);
      state.allFrames.sunglasses = state.allFrames.sunglasses.filter((item: any) => item._id != frame._id);
      state.frames.optical = state.frames.optical.filter((item: any) => item._id != frame._id);
      state.frames.sunglasses = state.frames.sunglasses.filter((item: any) => item._id != frame._id);
    },
    onDeselectFrame: (state: any, frame: any) => {
      state.selectedFrames = state.selectedFrames.filter((item: any) => item._id != frame._id);
      state.frames = { sunglasses: [], optical: [] };
      if (frame.category === 'O' && !state.allFrames.optical.map((optical: any) => optical._id).includes(frame._id)) {
        state.allFrames.optical.push(frame);
        state.allFrames.optical.sort(sortOrder);
      }
      if (frame.category === 'S' && !state.allFrames.sunglasses.map((sunglass: any) => sunglass._id).includes(frame._id)) {
        state.allFrames.sunglasses.push(frame);
        state.allFrames.sunglasses.sort(sortOrder);
      }
    }
  },
  getters: {
    'inventory/get': (state: any) => {
      return state.frames;
    },
    'getSelectedFrames': (state: any) => {
      return state.selectedFrames;
    }
  },
}