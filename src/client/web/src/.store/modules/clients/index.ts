import patients from '@/api/patients';

const sortOrder = (ent: any, elem: any) => {
  if (ent.lName.toLowerCase() < elem.lName.toLowerCase()) {
    return -1
  }
  if (ent.lName.toLowerCase() > elem.lName.toLowerCase()) {
    return 1
  }
  if ((ent.fName != undefined && elem.fName != undefined && ent.lName != undefined && elem.lName != undefined) && (ent.lName.toLowerCase() === elem.lName.toLowerCase()) && (ent.fName.toLowerCase() < elem.fName.toLowerCase())) {
    return -1
  }
  if ((ent.fName != undefined && elem.fName != undefined && ent.lName != undefined && elem.lName != undefined) && (ent.lName.toLowerCase() === elem.lName.toLowerCase()) && (ent.fName.toLowerCase() > elem.fName.toLowerCase())) {
    return 1
  }
  return 0
};

const nameFilter = (patient: any, name: string) => {
  if (name.length === 0)
    return true;
  return patient.fName.match(new RegExp(`^.*(${name}).*`, 'i'));
};

const familyNameFilter = (patient: any, familyName: string) => {
  if (familyName.length === 0)
    return true;
  return patient.lName.match(new RegExp(`^.*(${familyName}).*`, 'i'));
};

const glassFilter = (patient: any, glassCriteria: string) => {
  if (glassCriteria.length === 0)
    return true;
  return patient
    .glasses
    .some((glass: any) => glass.label.match(new RegExp(`^.*(${glassCriteria}).*`, 'i')));
};
const lentilFilter = (patient: any, lentilCriteria: string) => {
  if (lentilCriteria.length === 0)
    return true;
  return patient.lentils
    .some((lentil: any) => {
      return lentil.label.match(new RegExp(`^.*(${lentilCriteria}).*`, 'i'));
    })
};
const telFilter = (patient: any, telCriteria: string) => {
  if (telCriteria.length === 0)
    return true;
  return patient.entity.tel.some((tel: string) => tel.match(new RegExp(`^.*(${telCriteria}).*`, 'i')));
};

const updatedDayFilter = (patient: any, updatedDay: string) => {
  if (updatedDay.length === 0) {
    return true;
  }
  const date = new Date(patient.updatedAt);
  return date.getDate() == +updatedDay;
}

const updatedMonthFilter = (patient: any, updatedMonth: string) => {
  if (updatedMonth.length === 0) {
    return true;
  }
  const date = new Date(patient.updatedAt);
  return 1 + date.getMonth() == +updatedMonth;
}

const updatedYearFilter = (patient: any, updatedYear: string) => {
  if (updatedYear.length === 0) {
    return true;
  }
  const date = new Date(patient.updatedAt);
  return date.getFullYear() == +updatedYear;
}

const isInitialState = (patientCriteria: any, state: any): boolean => {
  if (Object
    .values(patientCriteria)
    .every((item: any) => item.length === 0)
    && (state.allPatients.length === 0)) {
    return true;
  } return false;
};

const isResetState = (patientCriteria: any, state: any): boolean => {
  if (Object
    .values(patientCriteria)
    .every((item: any) => item.length === 0)
    && (state.allPatients.length !== 0)) {
    return true;
  } return false;
};

export default {
  state() {
    return {
      clients: [],
      patients: [],
      allPatients: [],
      selectedClient: {}
    }
  },
  actions: {
  },
  mutations: {
    clients: (state: any, patientCriteria: any) => {
      const { updatedDay, updatedMonth, updatedYear, name, familyName, glass, lentil, tel } = patientCriteria;

      if (isInitialState(patientCriteria, state)) {
        patients
          .get()
          .then(data => {
            state.allPatients = data.sort(sortOrder);
          });
      } else if (isResetState([...updatedDay, ...updatedMonth, ...updatedYear, ...name, ...familyName, ...glass, ...lentil, ...tel], state)) {
        state.patients = [];
      }
      else {
        console.log(1);

        state.patients = state.allPatients
          .filter((patient: any) => {
            return nameFilter(patient, name)
              && familyNameFilter(patient, familyName)
              && glassFilter(patient, glass)
              && telFilter(patient, tel)
              && lentilFilter(patient, lentil)
              && updatedDayFilter(patient, updatedDay)
              && updatedMonthFilter(patient, updatedMonth)
              && updatedYearFilter(patient, updatedYear)
          })
          .sort(sortOrder);
        console.log(state.patients);

      }

    },
    onSelectClient: (state: any, client: any) => {
      state.selectedClient = client;
    }
  },
  getters: {
    'clients/get': (state: any) => {
      return state.patients;
    },
    'getSelectedClient': (state: any) => {
      return state.selectedClient;
    }
  },
};