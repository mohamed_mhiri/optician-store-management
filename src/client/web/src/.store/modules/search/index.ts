export default {
  state() {
    return {
      query: '',
    }
  },
  actions: {
    search: ({ commit, state }: any, { query }: any) => {
      commit(`${document.location.pathname.split('/')[1]}`, { query });
      commit('search', { query: '' });
    }
  },
  mutations: {
    search(state: any, { query }: any) {
      state.query = query;
    }
  },
  getters: {
    query(state: any) {
      return state.query;
    }
  },
};
