import receipts from '@/api/receipts';

export default {
  state() {
    return {
      receipts: {
        payed: [],
        notpayed: [],
        arranged: []
      },
    }
  },
  actions: {
  },
  mutations: {
    receipts: (state: any, { query }: any) => {
      if (query === '') {
        receipts
          .get()
          .then(data => {
            state.receipts.payed = data.filter((rec: any) => (rec.remains === 0 && rec.paid === '0'))
            state.receipts.notPayed = data.filter((rec: any) => (rec.remains != 0 && rec.paid === '0'))
            state.receipts.arranged = data.filter((rec: any) => (rec.paid === '1'));
          });
      } else {
        receipts
          .search(query)
          .then(data => {
            state.receipts.payed = data.filter((rec: any) => (rec.remains === 0 && rec.paid === '0'))
            state.receipts.notPayed = data.filter((rec: any) => (rec.remains != 0 && rec.paid === '0'))
            state.receipts.arranged = data.filter((rec: any) => (rec.paid === '1'));
          });
      }
    },
    createReceipt: (state: any, createReceipt: any) => {
      receipts.create(createReceipt)
      .then((result) => console.log(result));  
    }
  },
  getters: {
    'receipts/get': (state: any) => {
      return state.receipts;
    }
  },
};