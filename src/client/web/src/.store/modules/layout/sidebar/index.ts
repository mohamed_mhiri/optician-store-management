import { sidebarItems, Route } from '@/router/routes';

export default {
  state() {
    return {
      links: sidebarItems,
      link: Route,
    }
  },
  actions: {
  },
  mutations: {
    onSelectLink: (state: any, selectedLink: Route) => {
      console.log('store onSelectLink', state.links.filter((link: Route) => selectedLink.getLabel().includes(link.getLabel()))[0]);
      
      state.links.filter((link: Route) => selectedLink.getLabel().includes(link.getLabel()))[0].getChildRoutes().filter((route: Route) => route.getLabel() == selectedLink.getLabel()).cssClass = 'mm-active';
      console.log(state);
      
      //.cssClass = 'mm-active';
    }
  },
  getters: {
    links: (state: any) => {
      return state.links;
    },
  },
};