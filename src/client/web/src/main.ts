import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import router from './router';
import store from './.store';
import ToastPlugin from 'vue-toast-notification';
// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-bootstrap.css';

const pinia = createPinia();

createApp(App)
  .use(store)
  .use(pinia)
  .use(router)
  .use(ToastPlugin)
  .mount('#app');