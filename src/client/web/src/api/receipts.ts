export default {
  async get() {
    const data = await fetch('http://localhost:5700/api/receipt/getAll');
    return data.json();
  },
  async getOne(receiptId: any) {
    const data = await fetch(`http://localhost:5700/api/receipt/${receiptId}`);
    return data.json();
  },
  async search(query: string) {
    const data = await fetch(`http://localhost:5700/api/receipt/search/${query}`);
    return data.json();
  },
  async create(createReceipt: any) {
    console.log('createReceipt');
    console.log(createReceipt);
    
    const data = await fetch('http://localhost:5700/api/receipt', {
      method: 'POST',
      body: JSON.stringify(createReceipt),
      headers: { 'Content-Type': 'application/json' }
    });
    return data.json();
  },
  async update(updateReceipt: any) {
    console.log('updateReceipt');
    console.log(updateReceipt);
    
    const data = await fetch(`http://localhost:5700/api/receipt/${updateReceipt.data._id}`, {
      method: 'PUT',
      body: JSON.stringify(updateReceipt),
      headers: { 'Content-Type': 'application/json' }
    });
    return data.json();
  },
} 