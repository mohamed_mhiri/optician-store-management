export default {
  async getStatus() {
    const data = await fetch('http://localhost:5700/api/status');
    return data.json();
  },

  async updateStatus(val: boolean) {
    await fetch('http://localhost:5700/api/status', {
      method: 'PUT',
      body: JSON.stringify({val}),
      headers: { 'Content-Type': 'application/json' }
    })
  }
} 