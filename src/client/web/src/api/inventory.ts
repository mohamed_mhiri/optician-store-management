export default {
  async get() {
    const data = await fetch('http://localhost:5700/api/frame/getAll');
    return data.json();
  },
  async getOne(id: string) {
    const data = await fetch(`http://localhost:5700/api/frame/${id}`);
    return data.json();
  },
  async post(createFrame: any) {
    const data = await fetch('http://localhost:5700/api/frame', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(createFrame)
    });
    const jsonData = await data.json();
    return jsonData;
  },
  async search(query: string) {
    const data = await fetch(`http://localhost:5700/api/frame/search/${query}`);
    return data.json();
  },
}