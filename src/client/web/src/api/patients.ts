export default {
  async get() {
    const data = await fetch('http://localhost:5700/api/client/getAll');
    return data.json();
  },
  async search(query: string) {
    const data = await fetch(`http://localhost:5700/api/client/search/${query}`);
    return data.json();
  },
}