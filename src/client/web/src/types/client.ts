export interface PatientCriteria {
  updatedDay: string,
  updatedMonth: string,
  updatedYear: string,
  name: string,
  familyName: string,
  glass: string,
  lentil: string,
  tel: string,
}