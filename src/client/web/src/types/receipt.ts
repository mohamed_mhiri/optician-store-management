export interface ReceiptsState {
  receipts: {
    payed: Array<any>,
    notPayed: Array<any>,
    arranged: Array<any>
  },
}

export interface ReceiptState {
  receipt: any,
  isLoaded: boolean
}

export interface Receipt {
  _id: string | any,
  lName: string,
  fName: string,
  entity: {
    tel: Array<string>,
    rCylinder: string,
    rAxis: string,
    rAdd: string,
    rSphere: string,
    rHeight: string,
    rGap: string,
    lCylinder: string,
    lAxis: string,
    lAdd: string,
    lSphere: string,
    lHeight: string,
    lGap: string,
  },
  total: number,
  totalWithDiscount: number,
  downPayment: number,
  remains: number,
  paymentMethod: string,
  saleBy: Array<string>,
  frames: Array<any>,
  glasses: Array<any>,
  lentils: Array<any>,
}