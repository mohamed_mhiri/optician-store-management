#! /bin/sh

files=`ls $1 | grep '.vue' | sed 's/.vue//g'`
for file in $files
do
    mkdir $1/$file
    touch "$1/$file/index.css"
    touch "$1/$file/index.ts"
    touch "$1/$file/index.html"
    mv "$1/$file.vue" "$1/$file/index.vue"
done